# Macro Detector

Macro Detector is a web service that checks whether there is a macro inside a .doc/.xls file.

## Usage

Run MacroWebService.py using python and then use a client to send a file to check using POST request.
Postman app is recommended but given client (Client.py) is fine as well.

```python
python MacroWebService.py
python Client.py [file_name]
```

## Docker Usage

Build a Docker image using given Dockerfile and requirements.txt and run image.

### Build

```Docker
docker build -t flaskapp:latest .
```

### Run

```
docker run -p 5000:5000 flaskapp
docker run -d -p 5000:5000 flaskapp # run in the background
docker stop [CONTAINER ID] # stop service
```