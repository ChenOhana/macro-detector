from flask import Flask, request
from flask_restful import Api, Resource
from oletools.olevba import VBA_Parser, TYPE_OLE, TYPE_OpenXML, TYPE_Word2003_XML, TYPE_MHTML

app = Flask(__name__)
api = Api(app)

OK = 201 #response
PORT = 5000

class FilesManager(Resource):
	
	"""
	Function will write macro source code to file.
	:param: vbaparser
	:type: VBA_Parser
	:param: file
	:type: file pointer
	:return: None
	:rtype:
	"""
	
	def writeMacroDetails(self, vbaparser, file):
		macro = ""
		
		for (filename, stream_path, vba_filename, vba_code) in vbaparser.extract_macros(): #extract macros and details
			macro += ('-' * 79) + '\n'
			macro += 'Filename    :' + filename + '\n'
			macro += 'OLE stream  :' + stream_path + '\n'
			macro += 'VBA filename:' + vba_filename + '\n'
			macro += ('- ' * 40) + '\n'
			macro += vba_code #macro source code
			
		with open('Macro Source Code.txt', 'a') as macroDetails: #write data
			macroDetails.write("file: " + file.filename + '\n\n')
			macroDetails.write(macro)
			macroDetails.write(('-' * 79) + '\n')
	
	"""
	Function will get .doc/.xls file from client and check if it has macro in it.
	:param: None
	:type: 
	:return: None
	:rtype:
	"""
	
	def post(self):
		f = request.files.get('file') #get file from client
		vbaparser = VBA_Parser(f, data=f.read()) #get parser
		
		if vbaparser.detect_vba_macros(): #found macros
			data = 'VBA Macros found'
			self.writeMacroDetails(vbaparser, f) #write macro source code to file
		else:
			data = 'No VBA Macros found'
		return {'data': data}, OK

api.add_resource(FilesManager, '/')

if __name__ == '__main__':
	app.run(host='0.0.0.0',port=PORT,debug=True)