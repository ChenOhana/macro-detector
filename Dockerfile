FROM python:latest
	
COPY . .

RUN pip --no-cache-dir install -r requirements.txt

EXPOSE 5000

ENTRYPOINT ["python"]
CMD ["MacroWebService.py"]