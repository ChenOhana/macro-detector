import requests
import sys

def main():
	URL = "http://127.0.0.1:5000/"
	
	if len(sys.argv) == 1 or len(sys.argv) > 2: #wrong usage
		print ('Usage: Client.py [filename]')
		exit(1)
		
	with open(sys.argv[1], 'rb') as f:
		file = {'file': f}
		r = requests.post(URL, files = file) #send file to web service
		results = r.json()
		print(results['data']) #print results
	
if __name__ == "__main__": 
	main()